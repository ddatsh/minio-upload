package com.ddatsh.minio;

import io.minio.GetPresignedObjectUrlArgs;
import io.minio.MinioClient;
import io.minio.PostPolicy;
import io.minio.PutObjectArgs;
import io.minio.http.Method;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.InputStream;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Component
public class MinioComp {

    @Resource
    private MinioClient minioClient;

    @Resource
    private MinioConfiguration configuration;

    /**
     *  获取上传临时签名
     * @param fileName
     * @param time
     * @return
     */
    @SneakyThrows
    public Map getPolicy(String fileName, ZonedDateTime time) {

        PostPolicy postPolicy = new PostPolicy(configuration.getBucketName(), time);
        postPolicy.addEqualsCondition("key", fileName);

        Map<String, String> map = minioClient.getPresignedPostFormData(postPolicy);
        HashMap<String, String> map1 = new HashMap<>();
        map.forEach((k, v) -> {
            map1.put(k.replaceAll("-", ""), v);
        });
        map1.put("host", configuration.getUrl() + "/" + configuration.getBucketName());
        return map1;

    }

    /**
     * 获取上传文件的url
     */
    @SneakyThrows
    public String getPolicyUrl(String objectName, Method method, int time, TimeUnit timeUnit) {

        return minioClient.getPresignedObjectUrl(GetPresignedObjectUrlArgs.builder()
                .method(method)
                .bucket(configuration.getBucketName())
                .object(objectName)
                .expiry(time, timeUnit).build());

    }

    /**
     * 上传文件
     */
    @SneakyThrows
    public void upload(MultipartFile file, String fileName) {
        // 使用putObject上传一个文件到存储桶中。

        InputStream inputStream = file.getInputStream();
        minioClient.putObject(PutObjectArgs.builder()
                .bucket(configuration.getBucketName())
                .object(fileName)
                .stream(inputStream, file.getSize(), -1)
                .contentType(file.getContentType())
                .build());

    }

    /**
     * 根据filename获取文件访问地址
     */
    @SneakyThrows
    public String getUrl(String objectName, int time, TimeUnit timeUnit) {
        String url = minioClient.getPresignedObjectUrl(GetPresignedObjectUrlArgs.builder()
                .method(Method.GET)
                .bucket(configuration.getBucketName())
                .object(objectName)
                .expiry(time, timeUnit).build());

        return url;
    }
}