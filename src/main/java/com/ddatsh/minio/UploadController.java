package com.ddatsh.minio;

import io.minio.http.Method;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@RestController
@CrossOrigin
public class UploadController {

    @Resource
    MinioComp minioComp;

    @PostMapping("/upload")
    public Map upload(@RequestParam MultipartFile file,
                      @RequestParam String fileName) {
        minioComp.upload(file, fileName);
        String url = minioComp.getUrl(fileName, 5, TimeUnit.MINUTES);
        Map m = new HashMap<>();
        m.put("data", url);
        return m;
    }

    @GetMapping("/policy")
    public Map policy(@RequestParam String fileName) {
        Map m = new HashMap<>();
        m.put("data", minioComp.getPolicy(fileName, ZonedDateTime.now().plusDays(1L)));
        return m;
    }

    @GetMapping("/uploadUrl")
    public Map uploadUrl(@RequestParam String fileName) {
        Map m = new HashMap<>();
        String url = minioComp.getPolicyUrl(fileName, Method.PUT, 1, TimeUnit.DAYS);
        m.put("data", url);
        return m;
    }

    @GetMapping("/url")
    public Map url(@RequestParam String fileName) {
        Map m = new HashMap<>();
        m.put("data", minioComp.getUrl(fileName, 1, TimeUnit.DAYS));
        return m;
    }

}